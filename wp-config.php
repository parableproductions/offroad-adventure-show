<?php
define( 'WP_CACHE', true ); // Added by WP Rocket
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "offroad" );
/** MySQL database username */
define( 'DB_USER', "root" );
/** MySQL database password */
define( 'DB_PASSWORD', "root" );
/** MySQL hostname */
define( 'DB_HOST', "localhost" );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P|GA?1R&#$;w}Bozg@BM79Lodb{c};n-N1=dcG/]_.Tj[Q+>X8x0-YCyHnAsL3Vx' );
define( 'SECURE_AUTH_KEY',  '7EBr`)PDyj[R{x>i)nEb0!$:tAE7OaGL^eVcox7@45$m2/?($ s4CvikzLrL4UGD' );
define( 'LOGGED_IN_KEY',    'lF:FPSDk(YM=-6DIDLgoB,y{zE[(%7_6(7 ]jS6[ZkcasB2+iWs6FLv(R0.5pKH]' );
define( 'NONCE_KEY',        ':*eO+t`>M5zFLZ.qTZ*TpW|}UQPiX7Uo2}jRB,w|nI3mND<f WOMFHFoIX|BTanH' );
define( 'AUTH_SALT',        '24 ,C;sf7E=Q$o Y#.k<5%(da`%3AlWUgE}Sk;4!c:TH)0~U_-2Jts:fP3O|/M([' );
define( 'SECURE_AUTH_SALT', ':-cGYA,$<EuXY0V0Ja]4f*9e8?|]!Fsi`t8-%LW oC6vW<~HdJO?T03eW5WR:(%l' );
define( 'LOGGED_IN_SALT',   'd-_e$^)7_|ywBRkhK:f1ENE>4Y]dCXUs0<-A$=/#a7s2[{7TnOK|6:e6WtQaqMq*' );
define( 'NONCE_SALT',       'zS{+H].:Q.dPV_bNqFr)q~k8.&mw&{n`]/&4]J!EW1]yCh2UpHLvpqV3,LX_wYiW' );
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'oras_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
