********************************************************************************
* DUPLICATOR-PRO: Install-Log
* STEP-1 START @ 10:31:31
* VERSION: 1.3.22
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.2.24                                |5.6.40
OS__________________: Linux                                 |Linux
CREATED_____________: 2019-11-01 00:43:47
WP VERSION__________: 5.2.4
DUP VERSION_________: 1.3.22
DB__________________: 5.6.45
DB TABLES___________: 15
DB ROWS_____________: 4,434
DB FILE SIZE________: 2.12MB
********************************************************************************
SERVER INFO
PHP_________________: 5.6.40 | SAPI: cgi-fcgi
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: Apache
DOC ROOT____________: "/home/offroadadventure/public_html"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "http://offroadadventureshow.com.au/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ENGINE______: "shellexec_unzip"
SET DIR PERMS_______: 1
DIR PERMS VALUE_____: 1363
SET FILE PERMS______: 1
FILE PERMS VALUE____: 1204
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "20191101_offroadadventureshow_0190a0a4451a84463488_20191101004347_archive.zip"
SIZE________________: 380.54MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.


START ZIP FILE EXTRACTION SHELLEXEC >>> 
>>> Starting Shell-Exec Unzip:
Command: unzip -o -qq '/home/offroadadventure/public_html/20191101_offroadadventureshow_0190a0a4451a84463488_20191101004347_archive.zip' -d '/home/offroadadventure/public_html' 2>&1
<<< Shell-Exec Unzip Complete.
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------
PERMISSION UPDATES:
    -DIRS:  '755'
    -FILES: '644'

STEP-1 COMPLETE @ 10:31:35 - RUNTIME: 3.7039 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 10:31:47
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 5.7.28 -- Build Server: 5.6.45
FILE SIZE:	dup-database__0190a0a-01004347.sql (1.91MB)
TIMEOUT:	5000
MAXPACK:	268435456
SQLMODE:	ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[/home/offroadadventure/public_html/dup-installer/dup-installer-data__0190a0a-01004347.sql]
COLLATE FB:	Off
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	15
RENAMED TABLES:	0
QUERIES RAN:	144

oras_commentmeta: (0)
oras_comments: (1)
oras_duplicator_packages: (0)
oras_links: (0)
oras_options: (174)
oras_postmeta: (2886)
oras_posts: (737)
oras_term_relationships: (339)
oras_term_taxonomy: (29)
oras_termmeta: (50)
oras_terms: (29)
oras_usermeta: (32)
oras_users: (1)
oras_yoast_seo_links: (4)
oras_yoast_seo_meta: (83)
Removed '69' cache/transient rows

INSERT DATA RUNTIME: 0.1372 sec.
STEP-2 COMPLETE @ 10:31:47 - RUNTIME: 0.1394 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 10:31:55
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "Offroad Adventure Show"
postguid______________: false
fullsearch____________: false
path_old______________: "/home/parablep/oras.parableproductions.com.au/"
path_new______________: "/home/offroadadventure/public_html/"
siteurl_______________: "http://offroadadventureshow.com.au"
url_old_______________: "http://oras.parableproductions.com.au"
url_new_______________: "http://offroadadventureshow.com.au"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbhost________________: "localhost"
dbuser________________: "offroada_oras"
dbname________________: "offroada_oras"
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_username___________: ""
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: true
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "oras_commentmeta"________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "oras_comments"___________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_duplicator_packages"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "oras_links"______________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "oras_options"____________________________________[ROWS:   174][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_postmeta"___________________________________[ROWS:  2886][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_posts"______________________________________[ROWS:   737][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_term_relationships"_________________________[ROWS:   339][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_term_taxonomy"______________________________[ROWS:    29][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_termmeta"___________________________________[ROWS:    50][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_terms"______________________________________[ROWS:    29][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_usermeta"___________________________________[ROWS:    32][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_users"______________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_yoast_seo_links"____________________________[ROWS:     4][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"

EVALUATE TABLE: "oras_yoast_seo_meta"_____________________________[ROWS:    83][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/parablep/oras.parableproductions.com.au/" ==> "/home/offroadadventure/public_html/"
	SEARCH  2:"\/home\/parablep\/oras.parableproductions.com.au\/" => "\/home\/offroadadventure\/public_html\/"
	SEARCH  3:"%2Fhome%2Fparablep%2Foras.parableproductions.com.au%2F" => "%2Fhome%2Foffroadadventure%2Fpublic_html%2F"
	SEARCH  4:"\home\parablep\oras.parableproductions.com.au" ===> "/home/offroadadventure/public_html"
	SEARCH  5:"\\home\\parablep\\oras.parableproductions.com.au" => "\/home\/offroadadventure\/public_html"
	SEARCH  6:"%5Chome%5Cparablep%5Coras.parableproductions.com.au" => "%2Fhome%2Foffroadadventure%2Fpublic_html"
	SEARCH  7:"//oras.parableproductions.com.au" ================> "//offroadadventureshow.com.au"
	SEARCH  8:"\/\/oras.parableproductions.com.au" ==============> "\/\/offroadadventureshow.com.au"
	SEARCH  9:"%2F%2Foras.parableproductions.com.au" ============> "%2F%2Foffroadadventureshow.com.au"
	SEARCH 10:"https://offroadadventureshow.com.au" =============> "http://offroadadventureshow.com.au"
	SEARCH 11:"https:\/\/offroadadventureshow.com.au" ===========> "http:\/\/offroadadventureshow.com.au"
	SEARCH 12:"https%3A%2F%2Foffroadadventureshow.com.au" =======> "http%3A%2F%2Foffroadadventureshow.com.au"
--------------------------------------
SCANNED:	Tables:15 	|	 Rows:4365 	|	 Cells:31120 
UPDATED:	Tables:4 	|	 Rows:892 	|	 Cells:1049 
ERRORS:		0 
RUNTIME:	0.187400 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME ""offroada_oras""
	UPDATE DB_USER ""offroada_oras""
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	UPDATE WP_CACHE true
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache 'htaccess.orig' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Activated plugins (If they are activated) listed here will be deactivated: Array
(
    [0] => really-simple-ssl/rlrsssl-really-simple-ssl.php
    [1] => simple-google-recaptcha/simple-google-recaptcha.php
    [2] => js_composer/js_composer.php
)


====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 10:31:55 - RUNTIME: 0.2366 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No general notices
	SECTIONS: general

====================================
