<?php
/**
 * Offroad Adventure Show functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Offroad_Adventure_Show
 */

if ( ! function_exists( 'offroad_adventure_show_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function offroad_adventure_show_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Offroad Adventure Show, use a find and replace
		 * to change 'offroad-adventure-show' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'offroad-adventure-show', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'offroad-adventure-show' ),
			'menu-2' => esc_html__( 'footer-menu', 'offroad-adventure-show' ),

		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'offroad_adventure_show_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'offroad_adventure_show_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function offroad_adventure_show_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'offroad_adventure_show_content_width', 640 );
}
add_action( 'after_setup_theme', 'offroad_adventure_show_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function offroad_adventure_show_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'offroad-adventure-show' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'offroad-adventure-show' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'offroad_adventure_show_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function offroad_adventure_show_scripts() {

	//deregister outdated scripts
	wp_deregister_script('jquery');
	wp_deregister_script('jquery-ui-core');

	//deregister unused/unwanted scripts
	wp_deregister_script('parsley');
	wp_deregister_style("parsley-stype-css");
	wp_deregister_style("jetpack_css-css");
	wp_deregister_style("jetpack-widget-social-icons-styles-css");
	wp_deregister_style("buttons-css");
	wp_deregister_style("parsley-stype-css");

	//reenable outdated scripts with updated versions
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1', true );
	wp_enqueue_script( 'jquery-ui-core', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js", array('jquery'), '1.12.1', false );

    wp_enqueue_style( 'offroad-adventure-show-multisite-style', get_stylesheet_uri() );
    wp_enqueue_style( 'offroad-adventure-show-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(), '' );
    //wp_enqueue_style('main-style', get_template_directory_uri().'/dist/css/main.min.css');
	//wp_enqueue_style( 'offroad-adventure-show-custom', get_template_directory_uri() . '/assets/css/theme.css', array(), '1.0.3' );
	wp_enqueue_style( 'offroad-adventure-show-custom', get_template_directory_uri() . '/assets/css/theme.css', array(), '1.0.4');
    wp_enqueue_style( 'offroad-adventure-show-bootstrap', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css', array(), '' );
                 
    wp_enqueue_style( 'offroad-adventure-show-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700', array(), '' );
	wp_enqueue_script( 'offroad-adventure-show-multisite-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'offroad-adventure-show-multisite-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );    
    wp_enqueue_script( 'offroad-adventure-show-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '1.14.7', true );
    wp_enqueue_script( 'offroad-adventure-show-bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true ); 
    wp_enqueue_script( 'offroad-adventure-show-fontawesome', 'https://kit.fontawesome.com/34543f6683.js', array('jquery'), '5.9.0', true );
    wp_enqueue_script( 'offroad-adventure-show-fontawesome-slickjs', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '1.8.1', true );    

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    
}
add_action( 'wp_enqueue_scripts', 'offroad_adventure_show_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
* Custom Image Size.
*/
add_image_size( 'post-thumbnail', 1000, 450, true );
add_image_size( 'cooking-thumbnail', 390, 270, true );
add_image_size( 'cooking-big-thumbnail', 600, 270, true );


/**
 * Taxonomies.
 */
require get_template_directory() . '/inc/taxonomies.php';


add_theme_support( 'responsive-embeds' );
