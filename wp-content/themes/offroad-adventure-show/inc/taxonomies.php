<?php
// Register taxonomies
function create_state_taxonomy() {
    
  $labels = array(
    'name' => _x( 'State', 'taxonomy general name' ),
    'singular_name' => _x( 'State', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search States' ),
    'all_items' => __( 'All States' ),
    'parent_item' => __( 'Parent State' ),
    'parent_item_colon' => __( 'Parent State:' ),
    'edit_item' => __( 'Edit State' ),
    'update_item' => __( 'Update State' ),
    'add_new_item' => __( 'Add State' ),
    'new_item_name' => __( 'New State Name' ),
    'menu_name' => __( 'State' ),
  );
    
  register_taxonomy('state',array('tracks'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_state_taxonomy', 0 );

// Register taxonomies
function create_episode_category_taxonomy() {
    
  $labels = array(
    'name' => _x( 'Episode Category', 'taxonomy general name' ),
    'singular_name' => _x( 'Episode Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Episode Category' ),
    'all_items' => __( 'All Episode Category' ),
    'parent_item' => __( 'Parent Episode Category' ),
    'parent_item_colon' => __( 'Parent Episode Category:' ),
    'edit_item' => __( 'Edit Episode Category' ),
    'update_item' => __( 'Update Episode Category' ),
    'add_new_item' => __( 'Add Episode Category' ),
    'new_item_name' => __( 'New Episode Category Name' ),
    'menu_name' => __( 'Episode Category' ),
  );
    
  register_taxonomy('episode-category',array('episode'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_episode_category_taxonomy', 0 );

// Register taxonomies
function create_fishing_season_taxonomy() {
    
  $labels = array(
    'name' => _x( 'Fishing Season', 'taxonomy general name' ),
    'singular_name' => _x( 'Fishing Season', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Fishing Season' ),
    'all_items' => __( 'All Fishing Season' ),
    'parent_item' => __( 'Parent Fishing Season' ),
    'parent_item_colon' => __( 'Parent Fishing Season:' ),
    'edit_item' => __( 'Edit Fishing Season' ),
    'update_item' => __( 'Update Fishing Season' ),
    'add_new_item' => __( 'Add Fishing Season' ),
    'new_item_name' => __( 'New Fishing Season Name' ),
    'menu_name' => __( 'Fishing Season' ),
  );
    
  register_taxonomy('fishing-season',array('fishing'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_fishing_season_taxonomy', 0 );
