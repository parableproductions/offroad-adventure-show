<?php /* Template Name: Fishing */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Fishing -->
        <div class="col-md-8">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <!-- Latest Videos -->
            <h4>SEASON 5 FISHING</h4>
            <h5>WATCH THE LATEST VIDEO – 3PM FRIDAY!</h5>
            <p>
                Once again Starlo is out hunting for that next big fish. This season he is touring WA, Northern QLD
                and the Mid North Coast in search of his next tight line.
                Checkout what he has been up to in the videos below, as we upload more of his season 5 adventures at
                3pm on Fridays! </p>
            <div class="row">
                <?php
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'posts_per_page' => 8,
            'post_type' => 'fishing_videos',
            'orderby' => 'date',
            'paged' => $paged
        );
        query_posts($args); ?>
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="<?php the_permalink(); ?>">
                            <img class="card-img-top">
                            <?php if ( has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('cooking-thumbnail', array('class' => 'img-fluid')); ?>
                            <?php endif; ?>
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold"><?php the_title();?></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!-- Latest Seasons -->
            <div class="row mt-5">
                <?php
                      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'posts_per_page' => 8,
                            'post_type' => 'fishing',
                            'orderby' => 'date',
                            'paged' => $paged
                        );
                        query_posts($args); ?>
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3 pt-3">
                        <a href="<?php the_permalink(); ?>">
                            <div class="content">
                                <div class="content-overlay"></div>
                                <div class="content-image">
                                    <?php if ( has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('cooking-thumbnail', array('class' => 'img-fluid')); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="content-icon">
                                    <i class="far fa-file-alt fa-4x"></i>
                                </div>
                            </div>
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold"><?php the_title();?></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="text-center">
                <?php wp_pagenavi(); ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>            

        </div>
        <!-- /Fishing -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>