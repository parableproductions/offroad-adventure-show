<?php /* Template Name: Emags */ ?>

<?php get_header(); ?>

<div class="container">
    <div class="row">

        <div class="col-md-8">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h1><?php the_title();?></h1>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="row">
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <!--  -->
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="#">
                            <img class="card-img-top" src="https://offroad.local/wp-content/uploads/2020/11/ORAS-1-cover-768x544-1.jpg">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold">OFF ROAD ADVENTURE SHOW EMAG – TASMANIA – #1 2020</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <!--  -->
            </div>
            
            <div class="text-center">
                <?php wp_pagenavi(); ?>
            </div>

        <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>

        </div>

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>


<?php get_footer(); ?>