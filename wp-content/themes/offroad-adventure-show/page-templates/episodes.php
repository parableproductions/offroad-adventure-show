<?php /* Template Name: Episodes */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Home -->
        <div class="col-md-8">

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-leaderboard -->
                    <div id='div-gpt-ad-1572821278378-0'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1572821278378-0');
                        });
                        </script>
                    </div>
                </div>
            </div>

            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
            <?php
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'posts_per_page' => 8,
            'post_type' => 'episode',
            'orderby' => 'date',
            'paged' => $paged
        );
        query_posts($args); ?>
            <?php if ( have_posts() ) : ?>
            <div class="row pb-4">
                <?php while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">

                    <?php
			$terms = get_the_terms( $post->ID, 'episode-category' );
			if ( $terms && ! is_wp_error( $terms ) ) : 
				$episode_categories = array();
				foreach ( $terms as $term ) ?>
                    <div class="card px-3 pt-3">
                        <img class="card-img-top">
                        <?php echo the_field('episode_video');?>
                        <a href="<?php the_permalink(); ?>">
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <?php echo $term->name; ?>
                                <h6 class="card-title text-dark font-bold"><?php the_title();?></h6>
                            </div>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
                $i++;
                if ($i % 2 == 0) {echo '</div><div class="row pb-4">';}
                ?>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>


            <div class="text-center">
                <?php wp_pagenavi(); ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1572832884987-0');
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
    <!-- /Episodes -->

</div>

<?php get_footer(); ?>