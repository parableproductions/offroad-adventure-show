<?php /* Template Name: Partners */ ?>
<?php get_header(); ?>

<div class="parallax-partners">
    <div class="parallax-content">
        <div class="container">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<!-- Partners -->
<div class="bg-white">
    <div class="container">
        <div class="row">
            <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
            <?php
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'sponsors',
            'orderby' => 'date',
            'paged' => $paged
        );
        query_posts($args); ?>
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
            <div class="col-md-4">
                <a target="_blank" href="<?php the_field('sponsor_link'); ?>">
                    <div class="opacity">
                        <?php if ( has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                </a>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
</div>
<!-- /Partners -->

<?php get_footer(); ?>