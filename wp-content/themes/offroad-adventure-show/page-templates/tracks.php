<?php /* Template Name: Tracks */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row pl-2 pt-2">
        
        <!-- States -->
        <div class="col-md-8">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="row">
                <?php $args = array(
    "hide_empty" => 0,
    "parent" => 0,
    "type" => "tracks",   
    "taxonomy" => "state",   
    "orderby" => "name",
    "order" => "ASC" );
                    $categories = get_categories($args);
                    ?>
                <?php foreach ($categories as $category) : ?>
                <div class="col-md-4 mt-4 mb-4">
                    <?php 
                                $state_image = get_field('state_image', $category);
                                if( !empty($state_image) ): ?>
                    <img class="img-fluid" src="<?php echo $state_image['url']; ?>"
                        alt="<?php echo $state_image['alt']; ?>" />
                    <?php endif; ?>
                    <a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>">
                        <?php 
                                $state_button = get_field('state_button', $category);
                                if( !empty($state_button) ): ?>
                        <img class="opacity img-fluid" src="<?php echo $state_button['url']; ?>"
                            alt="<?php echo $state_button['alt']; ?>" />
                        <?php endif; ?>
                    </a>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_query(); ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>            

        </div>
        <!-- /State -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>