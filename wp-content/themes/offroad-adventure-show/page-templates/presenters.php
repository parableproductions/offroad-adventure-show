<?php /* Template Name: Presenters */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Presenter -->
        <div class="col-md-8">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="row">

                <?php
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'presenter',
            'orderby' => 'date',
            'paged' => $paged
        );
        query_posts($args); ?>
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <div class="col-md-6 pb-4">
                    <a target="_blank" href="<?php the_field('presenter_facebook') ?>">
                        <?php if ( has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('presenter-thumbnail', array('class' => 'img-fluid')); ?>
                    </a>
                    <h4 class="mt-3"><?php the_title();?></h4>
                    <h6><?php echo the_field("presenter_title"); ?></h6>
                    <p><?php echo the_field("presenter_description"); ?></p>
                </div>
                <?php endif; ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!-- /Presenter -->


        <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>            

        </div>
        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>


<?php get_footer(); ?>