<?php /* Template Name: GME */ ?>

<?php get_header(); ?>
    <div class="parallax-gme">
        <div class="parallax-content">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="gme-logo">
        <?php if ( has_post_thumbnail()) : ?>
            <?php the_post_thumbnail(); ?>
            </div>
            <?php endif; ?>
    </div>
</div>
    <div class="container mt-5">
        <!-- Sponsor -->

        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php endif; ?>
        <!-- /Sponsor -->
    </div>

<?php get_footer(); ?>