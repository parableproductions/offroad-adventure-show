<?php /* Template Name: Newsletter */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Newsletter -->
        <div class="col-md-8">

        <div class="row">
            <div class="col-md-12">
                <!-- /21695622747/oras-leaderboard -->
                <div id='div-gpt-ad-1572821278378-0'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572821278378-0'); });
                </script>
                </div>
            </div>
        </div>

            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="bg-white p-4">
                <h6 class="mb-3">SUBSCRIBE TO OUR MAILING LIST</h6>
                <p style="float: right; font-size: 12px;"><span class="mandatory-field">* </span>indicates required</p>
                <form class="js-cm-form" id="subForm" action="https://www.createsend.com/t/subscribeerror?description="
                    method="post"
                    data-id="30FEA77E7D0A9B8D7616376B90063231105F8FDEDE52E6C23930705B8DC7AA3D74D9E33D75B26E9590F95BFB0FFF41407FE02CAB9389455CF6C1D069D301F019">
                    <div>

                        <div><label>Email Address</label><span class="mandatory-field"> *</span><br>
                            <input aria-label="Email" class="js-cm-email-input" id="fieldEmail" maxlength="200"
                                name="cm-echkt-echkt" required="" type="email"></div>

                        <div><label>First Name</label><span class="mandatory-field"> *</span><br>
                            <input aria-label="FirstName" id="fieldFirstName" maxlength="200" name="cm-name"
                                required=""></div>

                        <div><label>Last Name</label><span class="mandatory-field"> *</span><br>
                            <input aria-label="LastName" id="fieldLastName" maxlength="200" name="cm-name" required="">
                        </div>

                        <div><label>Phone Number </label><br>
                            <input aria-label="Phone Number" id="fieldjiltltr" maxlength="200" name="cm-f-jiltltr">
                        </div>

                        <div><label>Age </label><br>
                            <input aria-label="Age" id="fieldjiltlty" maxlength="200" name="cm-f-jiltlty" type="number">
                        </div>

                        <div><label>State </label><br>
                            <input aria-label="State" id="fieldjiltltj" maxlength="200" name="cm-f-jiltltj"></div>

                        <div><label>Postcode </label><br>
                            <input aria-label="Postcode" id="fieldjiltltt" maxlength="200" name="cm-f-jiltltt"
                                type="number"></div>

                    </div><button class="btn-subscribe" type="submit">Subscribe</button>
                </form>
            </div>
            <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js">
            </script>

            <div class="row pt-4">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Newsletter -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>