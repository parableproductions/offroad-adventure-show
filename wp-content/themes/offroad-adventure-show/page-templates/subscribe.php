<form class="js-cm-form" id="subForm" action="https://www.createsend.com/t/subscribeerror?description=" method="post"
    data-id="30FEA77E7D0A9B8D7616376B90063231105F8FDEDE52E6C23930705B8DC7AA3D74D9E33D75B26E9590F95BFB0FFF41407FE02CAB9389455CF6C1D069D301F019">
    <h6>SIGN UP FOR MORE:</h6>
    <div class="row">
        <div class="col-md-4 mt-4">
            <input aria-label="Name" id="fieldName" maxlength="200" name="cm-name" required="" type="text"
                placeholder="Enter Your Name">
        </div>
        <div class="col-md-4 mt-4">
            <input aria-label="Email" class="js-cm-email-input" id="fieldEmail" maxlength="200" name="cm-echkt-echkt"
                required="" type="email" placeholder="Enter Your Email">
        </div>
        <div class="col-md-4">
            <input class="opacity" type="image"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/BUTTONS_newsletter.png" alt="Submit"
                width="250" height="90">
            </button>
        </div>
    </div>
</form>

<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js">
</script>