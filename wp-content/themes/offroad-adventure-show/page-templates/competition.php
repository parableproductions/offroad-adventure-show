<?php /* Template Name: Competitions */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Competitions -->
        <div class="col-md-8">

        <div class="row">
            <div class="col-md-12">
                <!-- /21695622747/oras-leaderboard -->
                <div id='div-gpt-ad-1572821278378-0'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572821278378-0'); });
                </script>
                </div>
            </div>
        </div>

            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="row">
                <?php                          
                        $args = array(
                            'posts_per_page' => 6,
                            'post_type' => 'competition' );
                        query_posts($args); ?>
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <div class="col-sm-12 pb-3 d-flex align-items-stretch">
                    <div class="card competition-card">
                        <img class="card-img-top">
                        <?php if ( has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('competition-thumbnail', array('class' => 'img-fluid')); ?>
                        <?php endif; ?>
                        <div class="card-body col-md-12 pt-1 px-0 pb-0">
                            <h5 class="card-title pt-1 text-center text-uppercase text-dark mb-2"> <?php the_title();?></h5>
                            <div class="post-date text-dark px-4 py-1"><?php the_field( 'competition_description' ); ?></div>
                            <div class="text-center">
                            <a target="_blank" href="<?php the_field('competition_link'); ?>" class="opacity"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/img/BUTTONS_COMPS.png"
                                    alt="Tracks" width="250" height="90"></a>
                                </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>


        <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>


        </div>

        <!-- /Competitions -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>