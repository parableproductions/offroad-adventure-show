<?php /* Template Name: Tough Dog */ ?>

<?php get_header(); ?>
<div class="bg-yellow">
    <div class="parallax-td">
        <div class="parallax-content">
            <?php 
$image = get_field('sponsor_logo');
if( !empty($image) ): ?>
            <img class="td-logo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?> </div>
    </div>
    <div class="container">
        <!-- Sponsor -->
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php endif; ?>
        <!-- /Sponsor -->
    </div>
</div>

<?php get_footer(); ?>