<?php /* Template Name: Insurance */ ?>
<?php get_header(); ?>

<div class="insurance-bg">
    <div class="container pt-5">
        <div class="row">

            <!-- Insurance -->
            <div class="col-md-8">
                <div class="mb-4">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>

                
        <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>


            </div>
            <!-- /Insurance -->

            <!-- Facebook -->
            <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
            <!-- /Facebook -->

        </div>
    </div>
</div>

<!-- Bottom Half -->
<div class="container pt-5">
    <!-- Insurance -->
    <h2 style="text-align: center;">WHY INSURE WITH CLUB 4X4?</h2>
    <table class="wp-block-table aligncenter">
        <tbody>
            <tr class="pb-3">
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    OFFROAD RECOVERY</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    PERSONAL GEAR COVERAGE</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    AUSTRALIA WIDE</td>
            </tr>
            <tr>
                <td>As part of your policy you have up to $1500 coverage towards the cost of recovering your
                    vehicle.*
                    Larger coverage amounts available.</td>
                <td>As part of your policy you have up to $2000 coverage for accidental loss or damage to
                    your personal
                    effects – anywhere in Australia, even if it’s not in your car at the time! Larger
                    coverage amounts
                    available.**</td>
                <td>That’s right we’ve said it – anywhere! On or offroad, you are completely covered for
                    where you take your
                    4WD</td>
            </tr>
            <tr>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    EVEN COVERS MODIFICATIONS</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    VEHICLE HIRE COVER</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    ESSENTIAL REPAIRS</td>
            </tr>
            <tr>
                <td>We understand what it takes to go touring. We will cover your rig!</td>
                <td>Does your insurer know how much it costs to rent a 4×4? Club4X4 will reimburse you for
                    up to $180 a day
                    for 21 days.</td>
                <td>We understand that your rig might cost more to get driveable in an emergency which is
                    why we cover up to
                    $1000 in emergency repairs to help you get to your destination.</td>
            </tr>
            <tr>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    MULTI POLICY DISCOUNT</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    FREE MAGAZINE SUBSCRIPTION</td>
                <td></td>
            </tr>
            <tr>
                <td>If you insure your caravan, camper trailer or slide on once you’ve insured your rig.</td>
                <td>What’s the point of being covered anywhere in Australia if you don’t know where to go?
                    Every policy
                    includes a subscription to Pat Callinans 4×4 Adventures Magazine!</td>
                <td></td>
            </tr>
        </tbody>
    </table>

    <figure class="wp-block-image"><img class="wp-image-671 aligncenter"
            src="http://oras.parableproductions.com.au/wp-content/uploads/2019/10/caravan-slide-on-camper-trailer-fifth-wheeler-1024x160.jpg"
            alt="" /></figure>

    <h2 style="text-align: center;"><strong>YOU’VE GOT THE BEST 4X4 POLICY,</strong><br
            data-rich-text-line-break="true" /><strong>NOW WHAT ABOUT YOUR HOME-AWAY-FROM-HOME?</strong></h2>
    <p style="text-align: center;">60% of 4X4 owners tow a caravan or camper trailer and 90% of Caravan or
        Camper Trailer
        owners tow using a 4X4. So why would you entrust your home-away-from-home to an insurer who is focused
        on an age
        bracket, or one that cannot cover all the accessories your tow vehicle?</p>
    <p style="text-align: center;">Club 4X4 understands the needs of the Australian adventurer, whether on road
        or off, you
        will always be well covered for your investment.</p>

    <table class="wp-block-table aligncenter">
        <tbody>
            <tr>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    COVERAGE WHERE YOU GO
                </td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    $750 BREAKDOWN OR ACCIDENT TOWING COVER</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    TRUE AGREED VALUE</td>
            </tr>
            <tr>
                <td>Full cover, anywhere in Australia, on or off-road.

                </td>
                <td>Not sure if your roadside assistance provider will tow your trailer if your vehicle breaks
                    down or is damaged? No problem.^</td>
                <td>Agreed or market value coverage, including cover for your modifications and accessories

                </td>
            </tr>
            <tr>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    CONTENTS COVER</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    LAID UP COVER</td>
                <td class="insurance-title"><span class="fa-stack circle-facebook">
                        <i class="fas insurance-circle fa-circle fa-stack-2x"></i>
                        <i class="fas fa-map-signs fa-stack-1x"></i>
                    </span>
                    ANNEXE COVER</td>
            </tr>
            <tr>
                <td>$1,000 automatic contents coverage ^^
                    Higher Coverages Available</td>
                <td>Why would you pay the same amount all year round if you don’t use it all year round? ***</td>
                <td>$2,000 automatic coverage for your annexe. ^^
                    Higher coverages available</td>
            </tr>
        </tbody>
    </table>
    <p>
        Insurance issued by The Hollard Insurance Company Pty Ltd, AFSL 241436 (Hollard). Club4x4 is an authorised
        representative of Hollard, AR No.1235616. The Offroad Adventure Show receives part of Club 4×4’s commission,
        paid by the product issuer. Consider the PDS at www.club4x4.com.au to decide if the product is right for you.
        Underwriting criteria apply.
    </p>
    <p>
        *This additional benefit is subject to an excess of $200 and one claim per policy period. Vehicle will be
        recovered to the nearest sealed road or <br>
        ** This additional benefit is subject to a $500 excess and a limit of $1000 per item applies. <br>
        ***Conditions apply and coverage during laid up cover is limited, see your PDS. <br>
        ^ This additional benefit is subject to a $100 Excess per claim. <br>
        ^^ This additional benefit is subject to a $200 Excess per claim. <br>
    </p>
</div>
<!-- /Bottom Half -->

<?php get_footer(); ?>