<?php /* Template Name: Cooking */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Cooking -->
        <div class="col-md-8">
            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="row">
                <?php
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'posts_per_page' => 8,
            'post_type' => 'cooking',
            'orderby' => 'date',
            'paged' => $paged
        );
        query_posts($args); ?>
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 d-flex align-items-stretch mb-4">
                    <div class="card px-3">
                        <a href="<?php the_permalink(); ?>">
                            <img class="card-img-top">
                            <?php if ( has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('cooking-thumbnail', array('class' => 'img-fluid')); ?>
                            <?php endif; ?>
                            <div class="card-body col-md-12 pb-0 pl-0">
                                <h6 class="card-title text-dark font-bold"><?php the_title();?></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="text-center">
                <?php wp_pagenavi(); ?>
            </div>


        <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572832884987-0'); });
                    </script>
                    </div>
                </div>
            </div>


        </div>
        <!-- /Recipes -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>