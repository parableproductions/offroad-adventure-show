<?php /* Template Name: Home */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row">

        <!-- Home -->
        <div class="col-md-8">

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-leaderboard -->
                    <div id='div-gpt-ad-1572821278378-0'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1572821278378-0');
                        });
                        </script>
                    </div>
                </div>
            </div>

            <div class="mb-4">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="mb-4">
                <?php the_field('home_video') ?>
            </div>

            <div class="bg-grey pl-4 pr-4 pb-0 pt-3">
                <?php get_template_part( 'page-templates/subscribe' ); ?>
            </div>

            <?php the_field('home_track_notes') ?>

            <a href="<?php echo site_url( '/vehicle/', 'https' ); ?>">
                <?php the_field('home_vehicles') ?>
            </a>

            <div class="row">
                <div class="col-md-12">
                    <!-- /21695622747/oras-footer -->
                    <div id='div-gpt-ad-1572832884987-0'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1572832884987-0');
                        });
                        </script>
                    </div>
                </div>
            </div>

        </div>
        <!--/Home -->

        <!-- Facebook -->
        <?php get_template_part( 'page-templates/oras-sidebar' ); ?>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>