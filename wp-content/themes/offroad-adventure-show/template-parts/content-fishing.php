<?php /* Template Name: Fishing */ ?>
<?php get_header(); ?>

<div class="bg-fishing">
    <div class="container pt-5 pb-5">
        <h1 class="text-center"><?php echo the_title(); ?></h1>
        <div class="row pl-2 pt-2">

            <!-- Fishing -->
            <div class="col-md-9">
                <div class="row">

                    <!-- Left Column -->
                    <div class="col-md-6">

                        <!-- Sub Title -->
                        <?php if( get_field('fishing_subtitle') ): ?>
                        <h3 class="fishing-subtitle"><?php the_field('fishing_subtitle'); ?></h3>
                        <?php endif; ?>
                        <!-- /Sub Title -->

                        <!-- Feature Image -->
                        <?php if ( has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('episode-thumbnail', array('class' => 'img-fluid')); ?>
                        <?php endif; ?>
                        <!-- /Feature Image -->

                        <!-- Sponsor -->
                        <div class="mt-4">
                            <?php the_field('fishing_sponsor'); ?>
                        </div>
                        <!-- /Sponsor -->

                        <!-- Guide -->
                        <?php if( get_field('fishing_guide')): ?>
                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-2 p-0">
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-circle circle-green fa-stack-2x"></i>
                                        <i class="far fa-hand-paper fa-stack-1x"></i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <?php the_field('fishing_guide'); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- /Guide -->

                        <!-- Company -->
                        <?php if( get_field('fishing_company')): ?>
                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-2 p-0">
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-circle circle-green fa-stack-2x"></i>
                                        <i class="fas fa-industry fa-stack-1x"></i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <?php the_field('fishing_company'); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- /Company -->

                        <!-- Company Details -->
                        <?php if( get_field('fishing_company_details')): ?>
                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-2 p-0">
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-circle circle-green fa-stack-2x"></i>
                                        <i class="fas fa-edit fa-stack-1x"></i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <?php the_field('fishing_company_details'); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- /Company Details -->

                        <!-- Stay -->
                        <?php if( get_field('fishing_stay')): ?>
                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-2 p-0">
                                    <span class="fa-stack fa-2x">
                                        <i class="fas fa-circle circle-green fa-stack-2x"></i>
                                        <i class="fas fa-home fa-stack-1x"></i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <?php the_field('fishing_stay'); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- /Stay -->
                    </div>
                    <!-- /Left Column -->

                    <!-- Right Column -->
                    <div class="col-md-6">

                        <!-- Text -->
                        <?php the_field('fishing_feature_list'); ?>
                        <!-- Text -->

                        <!-- Social Media -->
                        <div class="text-center">
                            <hr>
                            <p class="text-share">SHARE THIS POST</p>
                            <a class="acadp-social-link acadp-social-facebook"
                                href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($network_q->post->ID); ?>"
                                target="_blank"><i class="fab fa-facebook-square share-post fa-lg"></i></a>
                            <a class="acadp-social-link acadp-social-twitter"
                                href="https://twitter.com/intent/tweet?text=&amp;url=<?php the_permalink($network_q->post->ID); ?>"
                                target="_blank"><i class="fab fa-twitter-square share-post fa-lg"></i></a>
                            <a href="https://www.linkedin.com/cws/share?url=<?php the_permalink($network_q->post->ID); ?>"
                                target="_blank"><i class="fab fa-linkedin share-post fa-lg"></i></a>
                            <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink($network_q->post->ID); ?>"
                                target="_blank"><i class="fab fa-pinterest-square share-post fa-lg"></i></a>
                            <a href="http://www.reddit.com/submit?url=<?php the_permalink($network_q->post->ID); ?>"
                                target="_blank"><i class="fab fa-reddit-square share-post fa-lg"></i></a>
                            <a href="mailto:type%20email%20address%20here?subject=I%20wanted%20to%20share%20this%20post%20with%20you%20from%20<?php bloginfo('name'); ?>"
                                target="_blank"><i class="fas fa-envelope share-post fa-lg"></i></a>
                            <hr>
                        </div>
                        <!-- /Social Media -->

                    </div>
                    <!-- /Right Column -->

                </div>
            </div>
            <!-- /Fishing -->

            <div class="col-md-3">
            <?php get_template_part( 'page-templates/facebook' ); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>