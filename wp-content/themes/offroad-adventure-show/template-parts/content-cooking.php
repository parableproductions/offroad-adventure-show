<?php get_header(); ?>

<div class="container mt-4">
    <h4><?php the_title(); ?></h4>
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="mb-4">
                <?php if ( has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('cooking-big-thumbnail', array('class' => 'img-fluid')); ?>
                <?php endif; ?>
            </div>
            <div class="mb-4"><?php the_field('cooking_video') ?></div>
        </div>

        <div class="col-md-6">
            <h4>INGREDIENTS</h4>
            <div class="mb-4"><?php the_field('cooking_ingredients') ?></div>
            <h4>METHODS</h4>
            <div class="mb-4 pt-4"><?php the_field('cooking_method') ?></div>
        </div>
    </div>
</div>

<?php get_footer(); ?>