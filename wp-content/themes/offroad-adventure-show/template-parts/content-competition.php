<div id='woobox-root'></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//woobox.com/js/plugins/woo.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'woobox-sdk'));

</script>
<div class="container">
    <div class="row pl-2 pt-2">
        <div class="col-md-9">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">

                        <?php if ( 'post' === get_post_type() ) : ?>

                        <p class="entry-title">
                            <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'offroad_adventure_show' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">

                                <?php the_title(); ?></a>
                        </p>

                        <?php endif; ?>
                    </header><!-- .entry-header -->

                    <!-- post-body -->
                    <div class="post-body">

                        <section>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h2>
                                        <?php the_title(); ?>
                                    </h2>
                                    <?php if( get_field('competition_entry_form') ): ?>
                                    <p>
                                        <?php the_field('competition_entry_form'); ?>
                                    </p>
                                    <?php endif; ?>
                                    <?php if( get_field('competition_id') ): ?>
                                    <div class='woobox-offer' data-offer='<?php the_field('competition_id'); ?>'></div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php if( get_field('competition_form') ): ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php the_field('competition_form'); ?>
                                </div>
                            </div>
                            <?php endif; ?>
                        </section>


                    </div>
                </article><!-- #post-## -->

            