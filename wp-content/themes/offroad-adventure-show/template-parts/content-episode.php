<div class="container">
    <div class="row pl-2 pt-2">
    <h1><?php the_title();?></h1>

        <div class="col-md-9">

            <!-- Episodes -->
            <div class="text-center">
            <?php the_field('episode_video');?>
            </div>
            <?php the_field('episode_description');?>

        </div>
        <!-- /Episodes -->
        <div class="col-md-3">
        <?php get_template_part( 'page-templates/facebook' ); ?>
        </div>
    </div>
</div>