    $(document).ready(function () {
        $('.customer-logos').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    });

    $(document).ready(function () {
        var heights = $(".title").map(function () {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".title").height(maxHeight);
    });

    $(function () {
        $('.col-eq-height').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });

    $(function () {
        $('.card-eq-height').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });

    $(document).ready(function () {
        $(".btn-advsearch").click(function () {
            $("#advanced").slideToggle();
        });
    });

    $(".btn-advsearch").click(function () {
        $(this).text(function (i, v) {
            return v === 'Basic Search' ? 'Advanced Search' : 'Basic Search'
        })
    });

    $(function () {
        $("#dealership-list li").click(function (e) {
            e.preventDefault();
            var elem = $("#dealership-select");

            $(this).toggleClass("active");
            elem.val(
                $("#dealership-list li.active span").map(function () {
                    return $(this).text();
                }).get().join(",")
            );
        });
    });

    $(function () {
        $("#make-list li").click(function (e) {
            e.preventDefault();
            var elem = $("#make-select");

            $(this).toggleClass("active");
            elem.val(
                $("#make-list li.active span").map(function () {
                    return $(this).text();
                }).get().join(",")
            );
        });
    });

    $('ul.search-list').each(function () {

        var LiN = $(this).find('li').length;

        if (LiN > 3) {
            $('li', this).eq(2).nextAll().hide().addClass('toggleable');
            $(this).append('<li class="more">More...</li>');
        }

    });


    $('ul.search-list').on('click', '.more', function () {

        if ($(this).hasClass('less')) {
            $(this).text('View all').removeClass('less');
        } else {
            $(this).text('View less').addClass('less');
        }

        $(this).siblings('li.toggleable').slideToggle();

    });

    $("#clickToShowShopFront").hide().after('<button id="clickToShowShopFrontButton" class="btn btn-ccc btn-block"><span class="glyphicon glyphicon glyphicon-earphone"></span> Call Dealership</button>');
    $("#clickToShowShopFrontButton").click(function () {
        $("#clickToShowShopFront").show(function () {
            gtag('event', 'click', {
                'event_category': 'Page Events',
                'event_label': 'Phone Number',
                'value': 1
            });
        });
        $("#clickToShowShopFrontButton").hide();
    });

    $("#clickToShowContact").hide().after('<button id="clickToShowContactButton" class="btn btn-ccc btn-block"><span class="glyphicon glyphicon glyphicon-earphone"></span> Call Dealership</button>');
    $("#clickToShowContactButton").click(function () {
        $("#clickToShowContact").show(function () {
            gtag('event', 'click', {
                'event_category': 'Page Events',
                'event_label': 'Phone Number',
                'value': 1
            });
        });
        $("#clickToShowContactButton").hide();
    });

    $("#storewebsiteurl").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Website',
            'value': 1
        });
    });

    $("#storewebsiteurlbottom").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Website',
            'value': 1
        });
    });
    $("#contactformsubmit").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Contact Form',
            'value': 1
        });
    });
    $("#facebookshare").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Facebook Share',
            'value': 1
        });
    });
    $("#twittershare").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Twitter Share',
            'value': 1
        });
    });
    $("#googleshare").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Google Share',
            'value': 1
        });
    });
    $("#instagramlink").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Instagram Link',
            'value': 1
        });
    });
    $("#facebooklink").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Facebook Like',
            'value': 1
        });
    });
    $("#twitterlink").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Twitter Link',
            'value': 1
        });
    });
    $("#youtubelink").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'YouTube Link',
            'value': 1
        });
    });
    $("#strattonDisclaimer").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Stratton Disclaimer',
            'value': 1
        });
    });
    $("#strattonGetQuote").click(function () {
        gtag('event', 'click', {
            'event_category': 'Page Events',
            'event_label': 'Stratton Get Quote',
            'value': 1
        });
    });
