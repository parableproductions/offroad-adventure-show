<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Offroad_Adventure_Show
 */

?>

<div class="row bg-grey">
    <div class="col-md-4 text-right">
        <a href="<?php echo site_url( '/vehicle/', 'https' ); ?>">
            <img class="opacity" src="<?php echo get_template_directory_uri(); ?>/assets/img/BUTTONS_vehicles.png"
                alt="Vehicles" width="250" height="90">
        </a>
    </div>
    <div class="col-md-4 text-center">
        <a href="<?php echo site_url( '/cooking-recipes/', 'https' ); ?>">
            <img class="opacity" src="<?php echo get_template_directory_uri(); ?>/assets/img/BUTTONS_cooking.png"
                alt="Cooking" width="250" height="90">
        </a>
    </div>
    <div class="col-md-4 text-left">
        <a class="opacity" href="<?php echo site_url( '/tracks/', 'https' ); ?>"><img
                src="<?php echo get_template_directory_uri(); ?>/assets/img/BUTTONS_tracks.png" alt="Tracks" width="250"
                height="90"></a>
    </div>
</div>

<!-- /21695622747/oras-footer -->
<div id='div-gpt-ad-1571892564294-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1571892564294-0'); });
  </script>
</div>

<footer class="bg-footer">
    <div class="container">
        <div class="row social-media pt-4">
            <div class="col-md-12 text-center">

                <a href="https://www.facebook.com/offroadadventureshow" class="fa-stack circle-facebook fa-2x"
                    target="_blank">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-facebook-square fa-stack-1x"></i>
                </a>

                <a href="https://www.youtube.com/channel/UCnNdRuKUmvEKZONajwYzeQQ" class="fa-stack circle-youtube fa-2x"
                    target="_blank">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-youtube fa-stack-1x"></i>
                </a>

                <a href="https://vimeo.com/ondemand/oras02/156191512" class="fa-stack circle-vimeo fa-2x"
                    target="_blank">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-vimeo-square fa-stack-1x"></i>
                </a>

                <a href="https://www.instagram.com/offroadadventureshow/" class="fa-stack circle-instagram fa-2x"
                    target="_blank">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-instagram fa-stack-1x"></i>
                </a>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 text-center">
                <nav id="site-navigation" class="main-navigation pt-4 mb-4">
                    <button class="menu-toggle" aria-controls="primary-menu"
                        aria-expanded="false"><?php esc_html_e( 'Footer Menu', 'offroad-adventure-show' ); ?></button>
                    <?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'footer-menu',
			) );
			?>
                </nav>
            </div>
        </div>
    </div>

    <p class="all-rights font-light text-center text-white pb-4 m-0">&copy; <?php echo date("Y"); ?> Offroad Adventure Show.
        All Rights Reserved.</p>


    <?php wp_footer(); ?>
</footer>


<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0">
</script>

<script>
$('.tracks-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4000,

});
</script>


<script>
$('.hero-slider').slick({
    nextArrow: '<span class="hero-circle fa-stack fa-lg arrow-right" style="vertical-align: top;"><i class="fas fa-circle fa-stack-2x"></i><i class="fas fa-chevron-right fa-stack-1x fa-inverse"></i></span>',
    prevArrow: '<span class="hero-circle fa-stack fa-lg arrow-left" style="vertical-align: top;"><i class="fas fa-circle fa-stack-2x"></i><i class="fas fa-chevron-left fa-stack-1x fa-inverse"></i></span>',
    infinite: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    fade: true,
});
</script>




</body>

</html>