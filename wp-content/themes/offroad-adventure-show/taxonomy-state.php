<?php get_header(); ?>

<?php 
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
?>

<div class="container">
    <div class="mb-5">
    <h1><?php echo $term->name;?></h1>
    <p><?php echo $term->description;?></p>
    </div>
    <div class="row">

        <!-- Tracks -->
        <div class="col-md-9">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="pb-5">
            <h4><?php the_title(); ?></h4>
                    <?php if ( has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('competition-thumbnail', array('class' => 'img-fluid')); ?>
                    <?php endif; ?>
                    <p><?php echo the_field('tracks_description'); ?></p>

            <!-- Slider -->
            <div class="row">
                <div class="col-md-12">
                    <div class="tracks-slider">
                        <?php 
                            $images = get_field('tracks_gallery');
                            $size = 'large'; // (thumbnail, medium, large, full or custom size)
                            if( $images ): ?>
                        <?php foreach( $images as $image_id ): ?>
                        <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- /Slider -->

            <div class="row">

                <!-- Track Notes -->
                <a target="_blank" href="<?php the_field('track_notes') ?>">
                    <img class="pt-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/Track-Notes.jpg"
                        alt="Tracks"></a>
                <!-- /Track Notes -->

                <!-- Watch Here -->
                <a target="_blank" href=https://www.youtube.com/OffroadAdventureShow> <img class="mt-4 pb-4"
                    src="<?php echo get_template_directory_uri(); ?>/assets/img/Watch-it-here.jpg" alt="Tracks"></a>
                <!-- /Watch Here -->

            </div>
            <hr>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <!-- /Tracks -->

        <!-- Facebook -->
        <div class="col-md-3">
            <?php get_template_part( 'template-parts/content', 'facebook' ); ?>
        </div>
        <!-- /Facebook -->

    </div>
</div>

<?php get_footer(); ?>