<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Offroad_Adventure_Show
 */

get_header();
?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-9">
            <div class="mb-4">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <section class="error-404 not-found">
                            <header class="page-header">
                                <h4 class="page-title">
                                    <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'offroad-adventure-show' ); ?>
                                </h4>
                            </header><!-- .page-header -->
                            <div class="page-content">
                                <p><?php esc_html_e( "The page you are looking for is no longer here, or never existed in the first place (bummer). You can try searching for what you are looking for using the form below. If that still doesn't provide the results you are looking for, you can always start over from the home page." ); ?>
								</p>
								
                                <?php
					get_search_form();
										?>

                            </div><!-- .page-content -->
                        </section><!-- .error-404 -->
                    </main><!-- #main -->
                </div><!-- #primary -->
            </div>
        </div>
        <!-- Facebook -->
        <div class="col-md-3">
            <?php get_template_part( 'page-templates/facebook' ); ?>
        </div>
        <!-- /Facebook -->
    </div>
</div>

<?php
get_footer();