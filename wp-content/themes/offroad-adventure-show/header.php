<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Offroad_Adventure_Show
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {

    var mappingleaderboard1 = googletag.sizeMapping().

    addSize([992, 0], [
        [728, 90],
        [1, 1]
    ]). //desktop

    addSize([320, 0], [
        [320, 50],
        [1, 1]
    ]). //mobile

    build();

    googletag.defineSlot('/21695622747/oras-leaderboard', [[728, 90], 'fluid'], 'div-gpt-ad-1572821278378-0').defineSizeMapping(mappingleaderboard1).addService(googletag.pubads());

    googletag.defineSlot('/21695622747/oras-sidebar-top', [480, 320], 'div-gpt-ad-1572821816789-0').addService(googletag.pubads());
    googletag.defineSlot('/21695622747/oras-footer', [728, 90], 'div-gpt-ad-1572832884987-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<script data-ad-client="ca-pub-9754815191784332" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body <?php body_class(); ?>>

    <?php echo do_shortcode('[masterslider id="1"]');  ?>
    
    <header id="masthead" class="site-header">
        <?php ubermenu( 'main' , array( 'theme_location' => 'menu-1' ) ); ?>
    </header>

